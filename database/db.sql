CREATE TABLE albums (
    album_id INT PRIMARY KEY AUTO_INCREMENT,
    album_title VARCHAR(100) NOT NULL,
    artist VARCHAR(100) NOT NULL,
);

CREATE TABLE songs (
    song_id INT PRIMARY KEY AUTO_INCREMENT,
    album_id INT NOT NULL,
    song_title VARCHAR(100) NOT NULL,
    duration INT NOT NULL,
    downloads INT NOT NULL,
    plays INT NOT NULL,
    FOREIGN KEY (album_id) REFERENCES albums(album_id)
);

--PARA USAR LA REST API PRIMERO:

-- Insertar 2 álbumes
INSERT INTO albums (album_title, artist) VALUES ('Room on Fire', 'The Strokes');
INSERT INTO albums (album_title, artist) VALUES ('The Dark Side of the Moon', 'Pínk Floyd');
INSERT INTO albums (album_title, artist) VALUES ('Nevermind', 'Nirvana');

-- Insertar 2 canciones en el primer álbum
INSERT INTO songs (album_id, song_title, duration, downloads, plays)
VALUES (1, 'Reptilia', 180, 100, 500);
INSERT INTO songs (album_id, song_title, duration, downloads, plays)
VALUES (1, 'Under Control', 200, 200, 400);

-- Insertar 3 canciones en el segundo álbum
INSERT INTO songs (album_id, song_title, duration, downloads, plays)
VALUES (2, 'Money', 210, 300, 300);
INSERT INTO songs (album_id, song_title, duration, downloads, plays)
VALUES (2, 'Time', 170, 400, 200);
INSERT INTO songs (album_id, song_title, duration, downloads, plays)
VALUES (2, 'Eclipse', 150, 200, 100);
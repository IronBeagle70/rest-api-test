# Evaluación Práctica Typescript + NodeJs

## Stack
- Typescript
- Node.js
- Mysql

## Resumen

- Rest Api con Typescript, Node.js y Mysql.
- Esquema de la base de datos **Maestro/Detalle del producto**
- El **maestro** de producto es **Álbum** y el **detalle** son las **Canciones del Álbum**.
- Los campos del maestro Álbum cargados en caché son el género (**genre**) y año de lanzamiento (**release_year**)
- Airtable como servico externo. **Link de solo lectura**: https://airtable.com/shrJrnnNgwsju0FWp/tblDctiNKH9HukETq

## Dependencias de Producción
- **airtable**: Para utilizar los datos complementarios. Obtener los datos complementarios, crearlos y actualizarlos
- **mysql2**: Para utilizar consultas a la base de datos
- **express**: Framework de NodeJs

## Dependencias de desarrollo

- **dotenv**: Para las variables de entorno
- **lru-cache**: Para almacenar datos en caché
- **morgan**: Para registrar las solicitudes HTTP
- **ts-node-dev** : Similar al módulo nodemon pero para Typescript

# USABILIDAD

### Variables de Entorno
- **DEV_PORT** = Número de puerto 
- **DB_HOST** = Host del proyecto
- **DB_PORT** = Port de la database
- **DB_USER**= Usuario de la database
- **DB_PASSWORD**= Contraseña de la database
- **DB_DATABASE**= Nombre de la database
- **AIRTABLE_BASE_ID**= ID de la database externa en Airtable
- **AIRTABLE_API_KEY**= API KEY de para consumir la API de airtable

### Inicialmente se definen dos álbumes con sus canciones, dos datos de los álbumes en caché y dos datos externos en Airtable

### DATABASE
- El esquema esta definido en **/database/db.sql** . Junto con las consultas de inserción para usar la REST API.

### Maestro del Producto (Álbums):
#### (GET api/albums/ ):
- Obtiene todos los álbumes de la DB local
#### (GETBYID api/albums/:id ): 
- Obtiene un álbum por ID de la DB local, datos guardados en caché y datos complementarios de Airtable (image_url del Álbum) junto a sus canciones locales.
- El **objeto response final** está conformado por:
- **Database local:** ID, TÍTULO, ARTISTA (**album_id**, **album_title**, **artist**)
-  **Data caché:** GÉNERO y AÑO (**genre** y **release_year**)
- **Data Externa:** URL DE LA IMAGEN DEL ÁLBUM (**image_url**)
- **Detalle**: CANCIONES DEL ÁLBUM (**[songs]**)
#### (POST api/albums/ ): 
- Se puede crear datos del álbum en la DB local,  en memoria caché y en la DB externa.
#### (PUT api/albums/:id): 
- Se puede actualizar en la DB local, memoria caché y externa

### Detalle del Producto (Canciones):
- Solo operaciones locales
#### (GET api/songs/ ):
- Obtiene todos las canciones de la DB local
#### (GETBYID api/songs/:id ): 
- Obtiene una. canción por ID de la DB local
#### (GET api/albums/:id/songs ): 
- Obtiene todas las canciones de un álbum por ID
#### (GET /api/albums/:id1/songs/:id2): 
- Obtiene una canción de un álbum buscados por IDs
#### (POST /albums/:id/songs):
- Crea una canción en una álbum buscado por ID
#### (PUT  /api/albums/:id1/songs/:id2):
- Actualiza una canción de un álbum buscados por IDs

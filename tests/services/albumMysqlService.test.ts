
import { getPool } from '../../src/db.config';
import { Album } from '../../src/interface/Album';
import { AlbumMySqlService } from '../../src/services/album/mysql';

jest.mock('../../src/db.config', () => ({
  getPool: jest.fn(),
}));

describe('AlbumMySqlService', () => {
  let albumService: AlbumMySqlService;
  let pool: any;

  beforeEach(() => {
    pool = {
      execute: jest.fn().mockResolvedValue([[]]),
      query: jest.fn().mockResolvedValue([[]]),
    };
    (getPool as jest.Mock).mockReturnValue(pool);

    albumService = new AlbumMySqlService();
  });

  describe('getAll', () => {
    it('should call pool.execute with the correct SQL', async () => {
      await albumService.getAll();

      expect(pool.execute).toHaveBeenCalledWith(
        'SELECT B.*, A.* FROM songs B RIGHT JOIN albums A ON B.album_id = A.album_id ORDER BY B.album_id, B.song_id',
      );
    });

    it('should return the expected result', async () => {
      const expectedResult = [{
        song_id: 1,
        album_id: 1,
        song_title: 'Song',
        duration: 2,
        downloads:3,
        plays: 5,
        album_title: 'title',
        artist: 'Art',
        genre: 'g',
        release_year: 2023
      }];
      pool.execute.mockResolvedValue([expectedResult]);

      const result = await albumService.getAll();

      expect(result).toBeInstanceOf(Array);
      expect(result).toHaveLength(1);
    });
  });

  describe('getById', () => {
    it('should call pool.query with the correct SQL and parameters', async () => {
      const id = 123;
      await albumService.getById(id);

      expect(pool.query).toHaveBeenCalledWith(
        'SELECT * FROM albums WHERE album_id = ?',
        [id],
      );
    });

    it('should return the expected result', async () => {
      const expectedResult = [{ id: 1, name: 'Album 1' }];
      pool.query.mockResolvedValue([expectedResult]);

      const result = await albumService.getById(123);

      expect(result).toEqual(expectedResult[0]);
    });
  });

  describe('create', () => {
    it('should call pool.query with the correct SQL and parameters', async () => {
      const payload = { album_title: 'Album 1', artist: 'Artist 1' };
      await albumService.create(<any>payload);

      expect(pool.query).toHaveBeenCalledWith(
        'INSERT INTO albums (album_title, artist) VALUES (?,?)',
        [payload.album_title, payload.artist],
      );
    });
  });

});

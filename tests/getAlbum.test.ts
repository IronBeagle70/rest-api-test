//PRIMER TEST INICIAL (ERROR) 
// import { connect } from '../src/db.config';
// import { getAlbum } from '../src/controllers/album.controllers';
// import * as httpMocks from 'node-mocks-http';

// describe('GET /album/:id', () => {
//     it('retorna un album', async () => {
//         const req = httpMocks.createRequest({
//             method: 'GET',
//             url: '/album/1',
//             params: {
//                 id: 1,
//             },
//         });
//         const res = httpMocks.createResponse();

//         jest.spyOn(res, 'status').mockImplementation((): any => {
//             return {
//                 json: jest.fn().mockReturnValue({
//                     album_id: 1,
//                     album_name: 'album_name',
//                     artist_id: 1,
//                     album_cover: 'cover_url',
//                 }),
//             };
//         });

//         await getAlbum(req, res);

//         expect(res.statusCode).toBe(200);
//         const responseData = JSON.parse(res._getData());
//         expect(responseData).toEqual({
//             album_id: 1,
//             album_name: 'album_name',
//             artist_id: 1,
//             album_cover: 'cover_url',
//         });
//     });

//     it('retorna un error 404 si el album no existe', async () => {
//         const req = httpMocks.createRequest({
//             method: 'GET',
//             url: '/album/2',
//             params: {
//                 id: 2,
//             },
//         });
//         const res = httpMocks.createResponse();

//         jest.spyOn(res, 'status').mockImplementation((): any => {
//             return {
//                 json: jest.fn().mockReturnValue({ message: 'Album no encontrado' }),
//             };
//         });

//         await getAlbum(req, res);

//         expect(res.statusCode).toBe(404);
//         const responseData = JSON.parse(res._getData());
//         expect(responseData).toEqual({ message: 'Album no encontrado' });
//     });

//     it('retorna un error 500 si falla', async () => {
//         const req = httpMocks.createRequest({
//             method: 'GET',
//             url: '/album/1',
//             params: {
//                 id: 1,
//             },
//         });
//         const res = httpMocks.createResponse();

//         jest.spyOn(res, 'status').mockImplementation((): any => {
//             return {
//                 json: jest.fn().mockReturnValue({ message: 'Error en mostrar el album' }),
//             };
//         });

//         await getAlbum(req, res);

//         expect(res.statusCode).toBe(500);
//         const responseData = JSON.parse(res._getData());
//         expect(responseData).toEqual({ message: 'Error en mostrar el album' });
//     });
// });

//PRIMER TEST INICIAL

// import { getAlbums } from '../src/controllers/album.controllers';
// import * as httpMocks from 'node-mocks-http';

// describe('getAlbums', () => {
//     it('retorna la lista de todos los albums', async () => {
//         const req = httpMocks.createRequest({
//             method: 'GET',
//             url: '/albums'
//         });
//         const res = httpMocks.createResponse();

//         await getAlbums(req, res);

//         const responseData = JSON.parse(res._getData());
//         expect(responseData).toEqual([{
//             album_id: 1,
//             album_title: "primer album",
//             artist: "primer artista",
//             genre: "primer genero",
//             release_year: 2010
//         }]);
//         expect(res.statusCode).toBe(200);
//     });
// });

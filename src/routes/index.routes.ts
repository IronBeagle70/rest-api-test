import { Request, Response } from 'express';

import { Router } from 'express';

const router = Router();

router.get('/api', (req: Request, res: Response)=>{
    res.json({message: 'welcome to my app'});
});

export default router;

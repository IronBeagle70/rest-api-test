import { Router } from 'express';

import { getAlbums, createAlbum, getAlbum, updateAlbum } from '../controllers/album.controllers';  

const router = Router();

//get all albums
router.get('/albums', getAlbums);

//get album by ID (GETBYID)
router.get('/albums/:id', getAlbum); 

//create album
router.post('/albums/', createAlbum);

//update album
router.put('/albums/:id', updateAlbum);

export default router;
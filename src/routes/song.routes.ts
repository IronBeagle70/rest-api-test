import { Router } from 'express';

import { getSongs, getSong, getAlbumSongs, getAlbumSong, createAlbumSong, updateAlbumSong } from '../controllers/song.controllers';

const router = Router();

//get all songs
router.get('/songs', getSongs);

//get song by ID
router.get('/songs/:id',getSong);

//get all songs from one album
router.get('/albums/:id/songs', getAlbumSongs);

//get one song from one album
router.get('/albums/:id1/songs/:id2', getAlbumSong);

//create one song from one album
router.post('/albums/:id/songs/', createAlbumSong);

//update one song from one album 
router.put('/albums/:id1/songs/:id2', updateAlbumSong);

export default router;

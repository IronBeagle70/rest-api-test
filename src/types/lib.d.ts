/// <reference types="node" />
/// <reference types="./express" />

declare namespace NodeJS{
    interface ProcessEnv{
        /** 
         *  port dev
        */
        readonly DEV_PORT: number;
        /** 
         *  port host db
        */ 
        readonly DB_HOST: string;
        /** 
         *  port port db
        */ 
        readonly DB_PORT: number;
        /** 
         *  port username db
        */ 
        readonly DB_USER: string;
        /** 
         *  port password db
        */ 
        readonly DB_PASSWORD: string;
        /** 
         *  port database name
        */
        readonly DB_DATABASE: string;
        /** 
         *  db id airtable
        */
        readonly AIRTABLE_BASE_ID: string;
        /** 
         *  api key airtable
        */
        readonly AIRTABLE_API_KEY: string;
    }
}


import LRU from 'lru-cache';

const cache: LRU<number, {
    genre: string,
    release_year: number,
}> = new LRU({
    max: 100,
    ttl: 1000 * 60 * 60,
});

export function initCache() {
    cache.set(1, {
        genre: 'género en cache 1',
        release_year: 2001
    });
    cache.set(2, {
        genre: 'género en cache 2',
        release_year: 2002
    });
};

export function getCache() {
    return cache;
}
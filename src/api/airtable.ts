import Airtable from 'airtable';

import * as dotenv from 'dotenv';
dotenv.config();

const dbID = process.env.AIRTABLE_BASE_ID;
const apiKey = process.env.AIRTABLE_API_KEY;

let database: Airtable.Base;

try {
    if (!dbID) {
        throw new Error('No existe la variable de entorno del ID de la DB externa de Airtable');
    };
    if (!apiKey) {
        throw new Error('No existe la variable de entorno del apiKey de la DB externa de Airtable');
    };
    database = new Airtable({ apiKey: apiKey }).base(dbID);
} catch (error) {
    console.error(error);
};

if (dbID && apiKey) {
    database = new Airtable({ apiKey: apiKey }).base(dbID);
} else {
    console.error('dbID or apiKey is missing');
};

export function getAirtableDB() {
    return database;
}

const getAlbumRecords = async () => {
    try {
        const albumRecords = await database('albums')?.select({
            view: "Grid view"
        }).all();
        if (!albumRecords) {
            console.error('Error al recuperar registros de álbumes externos de Airtable');
            return [];
        };
        const externalAlbums = [];
        for (const record of albumRecords) {
            externalAlbums.push(record._rawJson);
        }
        return externalAlbums;
    } catch (error) {
        console.error(error);
        return [];
    };
};

export const main = async ()  => {
    try {
        const externAlbums = await getAlbumRecords();
        if (!externAlbums) {
            console.error('Error al recuperar álbumes externos de Airtable');
            return [];
        };
        return externAlbums;
    } catch (error) {
        console.error(error);
        return [];
    };
};


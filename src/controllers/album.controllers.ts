import { Request, Response } from 'express';

//interface
import {Album} from '../interface/Album';
import { Song } from '../interface/Song';

import { AlbumMySqlService } from '../services/album/mysql';
import { AlbumCacheService } from '../services/album/localCache';
import { AlbumAirtableService } from '../services/album/airtable';
import { SongMySqlService } from '../services/song/mysql';

//get all albums
export const getAlbums= async (req: Request,res: Response): Promise<Response | void>=>{
    try {
        const albumMysqlService = new AlbumMySqlService();
        const albumsRows = await albumMysqlService.getAll();
        if(!albumsRows.length){
            return res.status(404).json({ message: "Aún no existe álbumes" });
        };
        return res.status(200).json(albumsRows);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error en mostrar todos los álbumes" });
    };
};

//get album by ID (GETBYID)
export const getAlbum = async (req: Request,res: Response) =>{
    try {
        const albumID = Number(req.params.id);
        if (!albumID || isNaN(albumID)) {
            return res.status(400).json({ message: `El id del álbum es requerido y debe ser un número válido: ${albumID}` });
        };
        
        const mysqlService = new AlbumMySqlService();
        const mysqlResult = await mysqlService.getById(albumID);

        const airtableService = new AlbumAirtableService();
        const airtableResult = await airtableService.getById(albumID);

        const cacheService = new AlbumCacheService();
        const cacheResult = await cacheService.getById(Number(albumID));
        
        if(!mysqlResult || !airtableResult || !cacheResult){
            return res.status(404).json({ message: `Album ${albumID} no existe` });
        };
        const songMysqlService = new SongMySqlService();
        const songs = await songMysqlService.getByAlbumId(albumID);
        
        let mergedAlbum = new Album(
            mysqlResult.album_title,
            mysqlResult.artist,
            cacheResult?.genre,
            cacheResult?.release_year,
            mysqlResult.album_id,
            airtableResult.image_url,
        )
        mergedAlbum.songs = songs;
        return res.status(200).json(mergedAlbum);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error en mostrar todos el album" });
    };
};

//create album
export const createAlbum= async (req: Request,res: Response)=>{
    try {
        const {album_title, artist, genre, release_year, image_url } = req.body;
        if (!album_title || !artist || !image_url || genre || release_year ) {
            return res.status(400).json({ message: "Los campos de título, artista y url de la imagen del álbum son requeridos" });
        };
        const mysqlService = new AlbumMySqlService();
        const insertID = await mysqlService.create(req.body);

        const cacheService = new AlbumCacheService();
        cacheService.create({
            album_id: insertID,
            genre,
            release_year,
        });

        const airtableService = new AlbumAirtableService();
        await airtableService.create({ ...req.body, album_id: insertID });

        return res.status(201).json({message: "Nuevo álbum creado"});
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error interno al crear el álbum" });
    };
};

//update album
export const updateAlbum= async (req: Request,res: Response)=>{
    try {
        const id = Number(req.params.id);
        if(!id || isNaN(id)){
            return res.status(400).json({ message: `El id del álbum es requerido y debe ser un número válido: ${id}` });
        };
        const {album_title, artist, genre, release_year, image_url} = req.body;
        if (!album_title || !artist || !genre || !release_year  || !image_url) {
            return res.status(400).json({ message: "Todos los campos del álbum son requeridos" });
        };
        
        const albumData = new Album(album_title, artist, genre, release_year, id);

        const mysqlService = new AlbumMySqlService();
        const cacheService = new AlbumCacheService();
        const airtableService = new AlbumAirtableService();
        
        const resultArray = await mysqlService.getById(albumData.album_id!);

        if (!resultArray) {
            return res.status(404).json({ message: `Álbum ${id} no existe` });
        };
        await mysqlService.update(albumData);
        await cacheService.update({ ...req.body, album_id: id });
        await airtableService.update({ ...req.body, album_id: id });
        res.status(200).json({message: `Álbum ${id} actualizado`});
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error interno al actualizar el álbum" });
    };
};
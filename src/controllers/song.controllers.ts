import { Request, Response } from 'express';

//interface
import {Song} from '../interface/Song';
import { AlbumMySqlService } from '../services/album/mysql';
import { SongMySqlService } from '../services/song/mysql';

//get all songs
export const getSongs = async (req: Request,res: Response) =>{
    try {
        const mysqlService = new SongMySqlService();
        const songs = await mysqlService.getAll();
        if (!songs.length) {
            return res.status(404).json({ message: "Aún no hay canciones" });
        };
        return res.status(200).json(songs);       
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error en mostrar todas las canciones" });
    };
};

//get one song by Id
export const getSong = async (req: Request,res: Response) =>{
    try {
        const songID = Number(req.params.id);
        if (!songID || isNaN(songID)) {
            return res.status(400).json({ message: `El id de la canción debe ser un número válido: ${songID}` });
        };
        const mysqlService = new SongMySqlService();
        const songByID= await mysqlService.getById(songID);
        if(!songByID){
            return res.status(404).json({message: `Canción ${songID} no existe`});
        };
        return res.status(200).json(songByID);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error en mostrar la canción" });
    };
};

//get songs by album ID
export const getAlbumSongs = async (req: Request,res: Response) => {
    try {
        const albumID = Number(req.params.id);
        if (!albumID || isNaN(albumID) ) {
            return res.status(400).json({ message: `El id del álbum es requerido y debe ser un número válido: ${albumID}` });
        };
        const albumMysqlService = new AlbumMySqlService();
        const album = await albumMysqlService.getById(albumID);
        if(!album){
            return res.status(404).json({message: `El álbum ${albumID} no existe`});
        };
        const songMySqlService = new SongMySqlService();
        const songs = await songMySqlService.getByAlbumId(albumID);
        if(!songs.length){
            return res.status(404).json({message: `El álbum ${albumID} no tiene canciones`});
        };
        return res.status(200).json(songs);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error en mostrar todas las canciones del álbum" });
    };
};

//get one song by Id from one album by Id
export const getAlbumSong = async (req: Request,res: Response) => {
    try {
        const albumID = Number(req.params.id1);
        const songID = Number(req.params.id2);
        if (!albumID || !songID || isNaN(albumID) || isNaN(songID)) {
            return res.status(400).json({ message: `Los IDs de álbum y canción son requeridos y deben ser números válidos: ${albumID} - ${songID}` });
        };
        const albumMysqlService = new AlbumMySqlService();
        const album = await albumMysqlService.getById(albumID);
        if(!album){
            return res.status(404).json({message: `El álbum ${albumID} no existe`});
        };
        const songMySqlService = new SongMySqlService();
        const songs = await songMySqlService.getByAlbumId(albumID);
        if (!songs) {
            return res.status(404).json({ message: `No hay canciones en el álbum ${albumID}` });
        };
        const song = songs[songID - 1];
        if (!song) {
            return res.status(404).json({ message: `La canción ${songID} del álbum ${albumID} no existe` });
        };
        return res.status(200).json([song]);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error en mostrar la canción del álbum" });
    };
};

//post song
export const createAlbumSong = async (req: Request,res: Response) => {
    try {
        const albumID= Number(req.params.id);
        if (!albumID ||isNaN(albumID)) {
            return res.status(400).json({ message: `El id del álbum es requerido y debe ser un número válido: ${albumID}` });
        };
        const newSong:Song = req.body;
        const albumMySqlService = new AlbumMySqlService();
        const album = await albumMySqlService.getById(albumID);
        if (!album) {
            return res.status(404).json({ message: `El álbum ${albumID} no existe` });
        };
        if (!newSong.song_title || !newSong.duration) {
            return res.status(400).json({ message: "Título y duración de la canción son requeridos" });
        };
        const songMySqlService = new SongMySqlService();
        songMySqlService.create(albumID, newSong);        
        return res.status(201).json({message: `Nueva canción creada en el álbum ${albumID}`});
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error en crear la nueva canción" });
    };    
};

//update song
export const updateAlbumSong = async (req: Request,res: Response) => {
    try {
        const albumID= Number(req.params.id1);
        const songID = Number(req.params.id2);
        const updateSong:Song = req.body;
        if (!albumID || !songID || isNaN(albumID) || isNaN(songID)) {
            return res.status(400).json({ message: "Los IDs de álbum y canción son requeridos y deben ser números válidos" });
        };
        if (!updateSong.song_title || !updateSong.duration || !updateSong.downloads || !updateSong.plays) {
            return res.status(400).json({ message: "Todos los campos de la canción son requeridos" });
        };
        const albumMySqlService = new AlbumMySqlService();
        const album = await albumMySqlService.getById(albumID);
        if(!album){
            return res.status(404).json({ message: `El álbum ${albumID} no existe` });
        };
        const songMySqlService = new SongMySqlService();
        const songs = await songMySqlService.getByAlbumId(albumID);
        const song = songs[songID - 1];
        if (!song) {
            return res.status(404).json({ message: `Canción ${songID} del álbum ${albumID} no existe` });
        };
        await songMySqlService.update(updateSong, song);
        return res.status(200).json({message: `Canción ${songID} del álbum ${albumID} actualizada`}); 
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Error actualizar los datos de la canción" });
    };   
};

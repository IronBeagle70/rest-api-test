import * as dotenv from 'dotenv';

dotenv.config();

export const config = {
    dev:{
        appConfig:{
            port: process.env.DEV_PORT
        },
        dbconfig:{
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            dbName: process.env.DB_DATABASE
        }
    },
    // prod:{}
};
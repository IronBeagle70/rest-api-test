import { Song } from "./Song";

export class Album {
    album_id?: number;
    album_title?: string;
    artist?: string;
    genre?: string;
    release_year?: number;
    image_url?: string;
    songs?: Song[];

    constructor(album_title?: string, artist?: string, genre?: string, release_year?: number, album_id?: number, image_url?: string) {
        this.album_id = album_id;
        this.album_title = album_title;
        this.artist = artist;
        this.genre = genre;
        this.release_year = release_year;
        this.image_url = image_url;
        this.songs = [];
    }

    addSong(song: Song) {
        this.songs?.push(song);
    }
}

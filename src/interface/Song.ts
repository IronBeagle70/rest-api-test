export class Song {
    song_id?: number;
    album_id: number;
    song_title: string;
    duration: number;
    downloads: number;
    plays: number;

    constructor(album_id: number, song_title: string, duration: number, downloads: number, plays: number, song_id?: number) {
        this.album_id = album_id;
        this.song_id = song_id;
        this.song_title = song_title;
        this.duration = duration;
        this.downloads = downloads;
        this.plays = plays;
    }
}
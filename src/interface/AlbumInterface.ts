export interface AlbumInterface {
    album_id?: number,
    album_title: string,
    artist: string,
    genre: string,
    release_year: number,
    image_url?: string,
};

export type PartialAlbumInterface = Partial<AlbumInterface>
export interface SongInterface {
    song_id?: number;
    album_id: number;
    song_title: string;
    duration: number;
    downloads: number;
    plays: number;
};

export type PartialSongInterface = Partial<SongInterface>
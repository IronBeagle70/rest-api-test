import { createPool, Pool } from 'mysql2/promise';

import { config } from './config';

let pool: Pool;

export async function initDB() {
    const { host, port, user, password, dbName } = config.dev.dbconfig;

    try {
        if(!host || !port || !user || !password || !dbName){
            throw new Error('Error en la configuración de la DB');
        };
        pool = await createPool({
            host,
            port: Number(port),
            user,
            password,
            database: dbName
        });
        console.log('Base de datos conectada');
    } catch (error) {
        console.error('Error al conectar a la base de datos');
        throw error;
    };
};

export function getPool() {
    return pool;
}
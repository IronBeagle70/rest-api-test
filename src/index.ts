import express from 'express';

import morgan from 'morgan';

import fs from 'fs';

import * as path from 'path';

import {config} from './config';

import indexRoutes from './routes/index.routes'
import albumRoutes from './routes/album.routes';
import songRoutes from './routes/song.routes';
import { initDB } from './db.config';
import { initCache } from './cache.config';

const app = express();

initDB();
initCache();

const accessLogStream = fs.createWriteStream(path.join(__dirname,'../servicesResponses/response.txt'), {flags: 'a'});
app.use(morgan('method :method - url :url - statusCode :status - size :res[content-length] B - responseTime :response-time ms', {stream: accessLogStream}));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.use(indexRoutes);
app.use('/api',albumRoutes);
app.use('/api',songRoutes);

const port = config.dev.appConfig.port || 4000;
app.listen(port, ()=> console.log(`Server on port ${port}`));
import { Album } from "../interface/Album";
import { AlbumInterface, PartialAlbumInterface } from "../interface/AlbumInterface";
import { SongInterface, PartialSongInterface } from "../interface/SongInterface";

export interface AlbumServiceInterface {
    getById(id: number): Promise<PartialAlbumInterface>;
    getAll?(): Promise<PartialAlbumInterface[]>;
    create?(payload: AlbumInterface): Promise<number>;
    update?(payload: AlbumInterface): Promise<void>;
}

export interface SongServiceInterface {
    getById(id: number): Promise<PartialSongInterface>;
    getByAlbumId(id: number): Promise<PartialSongInterface[]>;
    getAll?(): Promise<PartialSongInterface[]>;
    create?(id:number, payload: SongInterface): Promise<void>;
    update?(payload: SongInterface, song: SongInterface): Promise<void>;
}
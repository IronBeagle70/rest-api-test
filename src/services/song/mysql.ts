import { Pool } from 'mysql2/promise';
import { getPool } from '../../db.config';
import { SongInterface, PartialSongInterface } from '../../interface/SongInterface'
import { Song } from '../../interface/Song';
import { SongServiceInterface } from '../servicesInterface'

export class SongMySqlService implements SongServiceInterface{
    private pool: Pool;

    constructor() {
        this.pool = getPool();
    }

    async getAll(): Promise<Song[]> {
        const [result] = await this.pool.query(
            'SELECT * FROM songs'
        );
        const resultArray = Object.values(result);
        return resultArray;
    }

    async getById(id: number): Promise<PartialSongInterface> {
        const [result] = await this.pool.query(
            'SELECT * FROM songs WHERE song_id=?',
            [id]
        );
        const resultSongByID = Object.values(result);
        return resultSongByID[0];
    };

    async getByAlbumId(id: number): Promise<Song[]> {
        const [result] = await this.pool.query(
            'SELECT * FROM songs WHERE album_id=?',
            [id]
        );
        const resultSongByID = Object.values(result);
        return resultSongByID;
    };

    async create(id: number, payload: SongInterface): Promise<void> {
        await this.pool.query(
            'INSERT INTO songs (album_id, song_title, duration, downloads, plays) VALUES (?,?,?,?,?)',
            [id, payload.song_title, payload.duration, payload.downloads || 0, payload.plays || 0]
        );
    }

    async update(payload: SongInterface, song: SongInterface): Promise<void> {
        await this.pool.query('UPDATE songs SET song_title=?, duration=?, downloads=?, plays=? WHERE song_id=?',
        [payload.song_title, payload.duration, payload.downloads, payload.plays, song.song_id]);
    }
    
}
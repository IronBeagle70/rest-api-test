import { Pool } from 'mysql2/promise';
import { getPool } from '../../db.config';
import { Album } from '../../interface/Album';
import { AlbumInterface, PartialAlbumInterface } from '../../interface/AlbumInterface'
import { Song } from '../../interface/Song';
import { AlbumServiceInterface } from '../servicesInterface'

export class AlbumMySqlService implements AlbumServiceInterface {
    private pool: Pool;

    constructor() {
        this.pool = getPool();
    }

    async getAll(): Promise<Album[]> {
        const [rows] = await this.pool.execute('SELECT B.*, A.* FROM songs B RIGHT JOIN albums A ON B.album_id = A.album_id ORDER BY B.album_id, B.song_id');

        const albumsRows = Object.values(rows);

        const albumsArray: Record<string, Album> = {};

        for(const album of albumsRows) {
            const {
                song_id,
                album_id,
                song_title,
                duration,
                downloads,
                plays,
                album_title,
                artist,
                genre,
                release_year
            } = album;

            if (!albumsArray[album_id!]) {
                albumsArray[album_id!] = new Album(album_title, artist, genre, release_year, album_id);
            }

            if (!song_id) continue;
            const song = new Song(album_id, song_title, duration, downloads, plays, song_id);
            albumsArray[album_id].addSong(song)
        };

        return Object.values(albumsArray);
    };

    async getById(id: number): Promise<PartialAlbumInterface> {
        const [AlbumByID] = await this.pool.query(
            'SELECT * FROM albums WHERE album_id = ?', 
            [id]
        );
        const resultAlbumByID = Object.values(AlbumByID)

        return resultAlbumByID[0];
    };

    async create(payload: AlbumInterface): Promise<number> {
        const [result] = await this.pool.query(
            'INSERT INTO albums (album_title, artist) VALUES (?,?)', 
            [payload.album_title, payload.artist]
        );
        const resultArray = Object.values(result)
        
        const insertId = resultArray[2];
        return insertId;
    };

    async update(payload: PartialAlbumInterface): Promise<void> {
        await this.pool.query(
            `UPDATE albums SET album_title=?, artist=? WHERE album_id=?`,
            [payload.album_title, payload.artist, payload.album_id]
        );
    }

};
import { AlbumInterface, PartialAlbumInterface } from '../../interface/AlbumInterface'
import { AlbumServiceInterface } from '../servicesInterface'
import LRU from 'lru-cache';
import { getCache } from '../../cache.config';

export class AlbumCacheService implements AlbumServiceInterface {
    private cache: LRU<number, {
        genre: string,
        release_year: number,
    }>;

    constructor() {
        this.cache = getCache()
    }

    async create(payload: Partial<AlbumInterface>): Promise<number> {
        await this.cache.set(payload.album_id as number, {
            genre: payload.genre!,
            release_year: payload.release_year!,
        })

        return payload.album_id as number;
    }

    async update(payload: AlbumInterface): Promise<void> {
        await this.cache.set(payload.album_id as number, {
            genre: payload.genre,
            release_year: payload.release_year,
        })
    }

    async getAll(): Promise<PartialAlbumInterface[]> {    
        return Array.from(this.cache.values());
    }

    async getById(id: number): Promise<PartialAlbumInterface> {
        const cacheAlbum = await this.cache.get(id)!;
        return cacheAlbum;
    }
}
import { AlbumInterface, PartialAlbumInterface } from '../../interface/AlbumInterface'
import { AlbumServiceInterface } from '../servicesInterface'
import Airtable from 'airtable';
import { getAirtableDB } from '../../api/airtable';

export class AlbumAirtableService implements AlbumServiceInterface {
    private db: Airtable.Base;

    constructor() {
        this.db = getAirtableDB();
    }

    async getById(id: number): Promise<PartialAlbumInterface> {
        const albumRecords = await this.db('albums')?.select({
            view: "Grid view"
        }).all();
        if (!albumRecords) {
            console.error('Error al recuperar registros de álbumes externos de Airtable');
            return <any>null;
        };
        const externalAlbums = [];
        for (const record of albumRecords) {
            externalAlbums.push(record._rawJson);
        }
        return externalAlbums[id-1]?.fields;
    }

    async create(payload: AlbumInterface): Promise<number> {
        await this.db('albums')?.create({
            album_id: payload.album_id,
            image_url: payload.image_url,
        });
        return payload.album_id!;
    }

    async update(payload: AlbumInterface): Promise<void> {
        const record = this.db('albums')
            .select({
            filterByFormula: `album_id = ${payload.album_id}`
        });
        const [result] = await record.firstPage()
        await this.db('albums')?.update(result.id,{
            image_url: payload.image_url
        });
    }
}